import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:kazokku_apps/core/configs/base_colors.dart';
import 'package:kazokku_apps/core/configs/base_fonts.dart';
import 'package:kazokku_apps/core/viewmodels/post_saved_provider.dart';
import 'package:kazokku_apps/ui/routes/route_page.dart';
import 'package:kazokku_apps/ui/widgets/components/shimmer/box_shimmer.dart';
import 'package:provider/provider.dart';

import '../../../core/models/user.dart';
import '../../widgets/components/appbar/appbar_component.dart';
import '../../widgets/components/shimmer/post_shimmer.dart';
import 'components/post_card.dart';

class PostSavedScreen extends StatefulWidget {
  const PostSavedScreen({super.key});

  @override
  State<PostSavedScreen> createState() => _PostSavedScreenState();
}

class _PostSavedScreenState extends State<PostSavedScreen> {
  @override
  void initState() {
    Future.delayed(Duration.zero, () async {
      Provider.of<PostSavedProvider>(context, listen: false).getlist();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PostSavedProvider>(
      builder: (context, provider, _) {
        return Scaffold(
          appBar: AppbarComponent.preferredSize(color: const Color(0XFF312b46)),
          backgroundColor: const Color(0XFFF9F9F9),
          body: RefreshIndicator(
            backgroundColor: const Color(0XFF312b46),
            color: Colors.white,
            onRefresh: () async {
              provider.getlist();
            },
            child: Stack(
              children: [
                Container(
                  width: double.infinity,
                  height: 110,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/images/bg1.png"),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 25, 20, 0),
                  child: Text(
                    "Posts Saved",
                    style: TextStyle(fontFamily: BaseFonts.poppinsBold, color: BaseColors.white, fontSize: 22),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 70, 0, 0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        provider.showLoading
                            ? const PostShimmer()
                            : provider.listOfData.length == 0 ? Container(
                              margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                              child: Column(
                                children: [
                                  Image.asset("assets/images/bg2.png", height: 300,),
                                  SizedBox(height: 20,),
                                  Text("Data Not Found")
                                ],
                              )
                            ) :
                            ListView.builder(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: provider.listOfData.length,
                                itemBuilder: (context, index) {
                                  var post = provider.listOfData[index];
                                  return PostCard(
                                    post: post,
                                    onTap: () {
                                      provider.saveFavorite(index);
                                    },
                                  );
                                },
                              ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget userCard(User user) {
    return Container(
      margin: const EdgeInsets.fromLTRB(20, 0, 20, 15),
      decoration: BoxDecoration(
        color: BaseColors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            blurRadius: 10,
            color: Colors.black.withOpacity(.01),
            offset: const Offset(0, 0),
          ),
        ],
      ),
      child: ElevatedButton(
        onPressed: () {
          RoutePage.userDetailScreen(user.id);
        },
        style: ButtonStyle(
          padding: MaterialStateProperty.all(const EdgeInsets.fromLTRB(20, 10, 20, 10)),
          elevation: MaterialStateProperty.all<double>(0),
          backgroundColor: MaterialStateProperty.all<Color>(BaseColors.white),
          overlayColor: MaterialStateProperty.all<Color>(BaseColors.overlayBtn),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
          ),
        ),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(105),
              child: CachedNetworkImage(
                fit: BoxFit.cover,
                width: 50,
                height: 50,
                imageUrl: user.picture,
                placeholder: (context, url) => Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: const BoxShimmer(
                    height: 80,
                    width: 100,
                  ),
                ),
                errorWidget: (context, url, error) => Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: const BoxShimmer(
                    height: 80,
                    width: 90,
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 20,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    user.title,
                    style: TextStyle(fontFamily: BaseFonts.interExtraBold, fontSize: 13, color: BaseColors.black),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Text(
                    "${user.firstName} ${user.lastName}",
                    style: TextStyle(fontSize: 14, color: BaseColors.black),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
