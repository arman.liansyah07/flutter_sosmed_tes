import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../../core/configs/base_colors.dart';
import '../../../../core/configs/base_fonts.dart';
import '../../../../core/models/post.dart';
import '../../../../core/utils/parse_utils.dart';
import '../../../widgets/components/shimmer/box_shimmer.dart';

class PostCard extends StatelessWidget {
  final Post post;
  final Function() onTap;
  final Function(String value)? onTapTag;

  const PostCard({required this.post, required this.onTap, this.onTapTag, super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
      margin: const EdgeInsets.fromLTRB(0, 0, 0, 20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            blurRadius: 10,
            color: Colors.black.withOpacity(.01),
            offset: const Offset(0, 0),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(105),
                child: CachedNetworkImage(
                  fit: BoxFit.cover,
                  width: 35,
                  height: 35,
                  imageUrl: post.owner.picture,
                  placeholder: (context, url) => Container(
                    margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                    child: const BoxShimmer(
                      width: 35,
                      height: 35,
                    ),
                  ),
                  errorWidget: (context, url, error) => Container(
                    margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                    child: const BoxShimmer(
                      width: 35,
                      height: 35,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      post.owner.firstName,
                      style: TextStyle(fontSize: 12, fontFamily: BaseFonts.poppinsSemiBold),
                    ),
                    const SizedBox(
                      height: 1,
                    ),
                    Text(
                      ParseUtils.castToDateFormat(post.publishDate),
                      style: const TextStyle(fontSize: 11),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              width: double.infinity,
              height: 180,
              imageUrl: post.image,
              placeholder: (context, url) => Container(
                margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: const BoxShimmer(
                  width: 80,
                  height: 80,
                ),
              ),
              errorWidget: (context, url, error) => Container(
                margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: const BoxShimmer(
                  width: 80,
                  height: 80,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Wrap(
            children: post.tags.map((e) {
              return GestureDetector(
                onTap: () {
                  onTapTag!(e);
                },
                child: Container(
                  margin: const EdgeInsets.only(right: 10),
                  padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                  decoration: BoxDecoration(color: BaseColors.blue, borderRadius: BorderRadius.circular(10)),
                  child: Text(
                    e,
                    style: TextStyle(fontFamily: BaseFonts.poppinsSemiBold, fontSize: 10, color: Colors.white),
                  ),
                ),
              );
            }).toList(),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            post.text,
            style: const TextStyle(fontSize: 12),
          ),
          const SizedBox(
            height: 5,
          ),
          Row(
            children: [
              GestureDetector(
                onTap: onTap,
                child: Icon(
                  Icons.favorite,
                  color: post.isSave ? BaseColors.danger : Colors.grey[400]!,
                  size: 20,
                ),
              ),
              const SizedBox(
                width: 5,
              ),
              Text(
                "${post.likes} Likes",
                style: TextStyle(fontSize: 12, fontFamily: BaseFonts.poppinsMedium, color: BaseColors.danger),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
