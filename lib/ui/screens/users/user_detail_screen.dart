import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:kazokku_apps/core/viewmodels/user_post_provider.dart';
import 'package:kazokku_apps/ui/screens/posts/components/post_card.dart';
import 'package:kazokku_apps/ui/widgets/components/shimmer/post_shimmer.dart';
import 'package:kazokku_apps/ui/widgets/components/shimmer/user_detail_shimmer.dart';
import 'package:provider/provider.dart';

import '../../../core/configs/base_colors.dart';
import '../../../core/configs/base_fonts.dart';
import '../../../core/utils/parse_utils.dart';
import '../../../core/viewmodels/user_providers.dart';
import '../../routes/route_page.dart';
import '../../widgets/components/appbar/appbar_component.dart';
import '../../widgets/components/loading/loading_pagination.dart';
import '../../widgets/components/shimmer/box_shimmer.dart';

class UserDetailScreen extends StatefulWidget {
  final String id;
  const UserDetailScreen({
    required this.id,
    super.key,
  });

  @override
  State<UserDetailScreen> createState() => _UserDetailScreenState();
}

class _UserDetailScreenState extends State<UserDetailScreen> {
  ScrollController scrollController = ScrollController();

  _scrollListener() async {
    if (scrollController.offset >= scrollController.position.maxScrollExtent && !scrollController.position.outOfRange) {
      Provider.of<UserPostProvider>(context, listen: false).getlist(widget.id, isShowPagination: true);
    }
  }

  @override
  void initState() {
    scrollController.addListener(_scrollListener);
    Future.delayed(Duration.zero, () async {
      Provider.of<UserProvider>(context, listen: false).getDetail(widget.id);
      Provider.of<UserPostProvider>(context, listen: false).getlist(widget.id);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserProvider>(
      builder: (context, provider, _) {
        return Consumer<UserPostProvider>(
          builder: (context, userPostProvider, _) {
            return Scaffold(
              appBar: AppbarComponent.preferredSize(color: const Color(0XFF312b46)),
              backgroundColor: const Color(0XFFF9F9F9),
              body: provider.user == null
                  ? const UserDetailShimer()
                  : RefreshIndicator(
                      backgroundColor: const Color(0XFF312b46),
                      color: Colors.white,
                      onRefresh: () async {
                        provider.getDetail(widget.id);
                        userPostProvider.getlist(widget.id);
                      },
                      child: Stack(
                        children: [
                          Container(
                            width: double.infinity,
                            height: 120,
                            decoration: const BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("assets/images/bg1.png"),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10),
                                bottomRight: Radius.circular(10),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: Row(
                              children: [
                                InkWell(
                                  onTap: () {
                                    RoutePage.back();
                                  },
                                  child: Container(
                                    padding: const EdgeInsets.fromLTRB(20, 20, 10, 10),
                                    child: Icon(
                                      Icons.arrow_back_ios,
                                      color: BaseColors.white,
                                      size: 15,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    height: 40,
                                    margin: const EdgeInsets.fromLTRB(10, 15, 40, 0),
                                    decoration: BoxDecoration(
                                      color: BaseColors.white,
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: TextField(
                                      controller: userPostProvider.search,
                                      onChanged: (value) {
                                        userPostProvider.getlist(widget.id, isSearch: true);
                                      },
                                      cursorColor: BaseColors.black,
                                      style: TextStyle(
                                        color: BaseColors.black,
                                        fontSize: 13,
                                      ),
                                      decoration: InputDecoration(
                                        hintText: "Search in ${provider.user!.firstName} posts",
                                        contentPadding: const EdgeInsets.fromLTRB(20, 0, 10, 10),
                                        border: const OutlineInputBorder(),
                                        hintStyle: TextStyle(color: BaseColors.black, fontSize: 13),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: BaseColors.blue, width: 0.5),
                                          borderRadius: BorderRadius.circular(5),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: BaseColors.blue, width: 0.5),
                                          borderRadius: BorderRadius.circular(5),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 60, 0, 0),
                            child: SingleChildScrollView(
                              controller: scrollController,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                    padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                                    width: double.infinity,
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        ClipRRect(
                                          borderRadius: BorderRadius.circular(105),
                                          child: CachedNetworkImage(
                                            fit: BoxFit.cover,
                                            width: 80,
                                            height: 80,
                                            imageUrl: provider.user!.picture,
                                            placeholder: (context, url) => Container(
                                              margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                                              child: const BoxShimmer(
                                                width: 80,
                                                height: 80,
                                              ),
                                            ),
                                            errorWidget: (context, url, error) => Container(
                                              margin: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                                              child: const BoxShimmer(
                                                width: 80,
                                                height: 80,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          padding: const EdgeInsets.all(5),
                                          decoration: BoxDecoration(color: const Color(0XFFF9F9F9), borderRadius: BorderRadius.circular(10)),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              const SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Expanded(
                                                    child: Text(
                                                      "${provider.user!.title} ${provider.user!.firstName} ${provider.user!.lastName} ",
                                                      style: TextStyle(fontFamily: BaseFonts.poppinsBold, fontSize: 18),
                                                      textAlign: TextAlign.left,
                                                    ),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      provider.addFriend();
                                                    },
                                                    child: provider.user!.isFriend
                                                        ? Icon(
                                                            Icons.playlist_add_check_circle_sharp,
                                                            color: BaseColors.success,
                                                          )
                                                        : Icon(Icons.group_add_sharp),
                                                  )
                                                ],
                                              ),
                                              const SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Expanded(
                                                    flex: 4,
                                                    child: bioItem(
                                                      "assets/icons/iconGender.png",
                                                      "${provider.user!.gender}",
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 6,
                                                    child: bioItem(
                                                      "assets/icons/iconDob.png",
                                                      ParseUtils.castToDateFormat(provider.user!.dateOfBirth),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Expanded(
                                                    flex: 4,
                                                    child: bioItem(
                                                      "assets/icons/iconJoin.png",
                                                      ParseUtils.castToDateFormat(provider.user!.registerDate),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 6,
                                                    child: bioItem(
                                                      "assets/icons/iconEmail.png",
                                                      "${provider.user!.email}",
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              const SizedBox(
                                                height: 5,
                                              ),
                                              bioItem(
                                                "assets/icons/iconAddress.png",
                                                "${provider.user!.address!.country}, ${provider.user!.address!.city}, ${provider.user!.address!.state}, ${provider.user!.address!.street}",
                                              ),
                                              const SizedBox(
                                                height: 10,
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Listing Posts ",
                                          style: TextStyle(fontFamily: BaseFonts.poppinsBold, fontSize: 18),
                                          textAlign: TextAlign.center,
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        userPostProvider.showLoading
                                            ? const PostShimmer()
                                            : ListView.builder(
                                                shrinkWrap: true,
                                                physics: const NeverScrollableScrollPhysics(),
                                                itemCount: userPostProvider.listOfData.length,
                                                itemBuilder: (context, index) {
                                                  var post = userPostProvider.listOfData[index];
                                                  return PostCard(
                                                    post: post,
                                                    onTap: () {
                                                      userPostProvider.saveFavorite(index);
                                                    },
                                                  );
                                                },
                                              ),
                                        !userPostProvider.showLoadingPage ? Container() : const LoadingPagination()
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
            );
          },
        );
      },
    );
  }

  Widget bioItem(String icon, String value) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
      child: Row(
        children: [
          Image.asset(
            icon,
            width: 20,
          ),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: Text(
              value,
              style: const TextStyle(fontSize: 11),
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }
}
