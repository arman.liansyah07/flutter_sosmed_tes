import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../core/configs/base_colors.dart';
import '../../../core/viewmodels/setting_provider.dart';
import '../../widgets/components/appbar/appbar_component.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({super.key});

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  

  GlobalKey btnDashboard1 = GlobalKey();
  GlobalKey btnDashboard2 = GlobalKey();
  GlobalKey btnDashboard3 = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Consumer<SettingProvider>(
      builder: (context, provider, _) {
        return Scaffold(
          appBar: AppbarComponent.preferredSize(color: const Color(0XFF312b46)),
          backgroundColor: const Color(0XFFF9F9F9),
          body: provider.listOfPages[provider.index],
          bottomNavigationBar: Stack(
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(20, 0, 20, 20),
                height: 55,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: const Color(0XFF312b46),
                  borderRadius: BorderRadius.circular(50),
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 5,
                      color: Colors.black.withOpacity(.05),
                      offset: const Offset(0, 0),
                    ),
                  ],
                ),
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: FittedBox(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      iconNavbar(
                        provider,
                        icon: "assets/icons/iconHome.png",
                        indexValue: 0,
                        key: btnDashboard1,
                      ),
                      iconNavbar(
                        provider,
                        icon: "assets/icons/iconPost.png",
                        indexValue: 1,
                        key: btnDashboard2,
                      ),
                      iconNavbar(
                        provider,
                        icon: "assets/icons/iconWhitelist.png",
                        indexValue: 2,
                        key: btnDashboard3,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget iconNavbar(SettingProvider settingProvider, {required String icon, required int indexValue, required GlobalKey key}) {
    return ElevatedButton(
      key: key,
      onPressed: () {
        settingProvider.setPage(indexValue);
      },
      style: ButtonStyle(
        padding: MaterialStateProperty.all(const EdgeInsets.all(0)),
        alignment: Alignment.center,
        elevation: MaterialStateProperty.all<double>(0),
        backgroundColor: MaterialStateProperty.all<Color>(settingProvider.index == indexValue ? BaseColors.warning : Colors.transparent),
        overlayColor: MaterialStateProperty.all<Color>(BaseColors.warning),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0.0),
          ),
        ),
      ),
      child: Column(
        children: [
          const SizedBox(
            height: 15,
          ),
          Image.asset(
            icon,
            height: 18,
          ),
          const SizedBox(
            height: 15,
          ),
        ],
      ),
    );
  }
}
