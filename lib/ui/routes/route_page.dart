import 'package:flutter/material.dart';
import 'package:kazokku_apps/ui/screens/users/user_detail_screen.dart';
import '../../core/utils/navigation_utils.dart';

class RoutePage {
   static back() {
    navigate.navigatorKey.currentState!.pop();
  }

  static userDetailScreen(String id) {
    navigate.navigatorKey.currentState!.push(
      MaterialPageRoute(
        builder: (context) => UserDetailScreen(id: id),
      ),
    );
  }
}
