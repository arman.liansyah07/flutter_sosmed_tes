
import 'package:get_it/get_it.dart';
import '../../core/utils/navigation_utils.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => NavigationUtils());
}