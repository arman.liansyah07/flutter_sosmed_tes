import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppbarComponent {

  AppbarComponent._();
  
  static preferredSize({color = const Color(0XFF312b46)}) {
    return PreferredSize(
      preferredSize: const Size.fromHeight(0.0),
      child: AppBar(
        systemOverlayStyle: systemUiOverlayStyle(color),
        elevation: 0,
        backgroundColor: color,
      ),
    );
  }

  static SystemUiOverlayStyle systemUiOverlayStyle(color) {
    return SystemUiOverlayStyle(
      statusBarColor: color,
      systemNavigationBarColor: color,
      systemNavigationBarDividerColor: color,
      systemNavigationBarIconBrightness: colorNavbar(color),
      statusBarBrightness: colorNavbar(color),
      statusBarIconBrightness: colorNavbar(color),
      
    );
  }

  static Brightness colorNavbar(Color value) {
    try {
      if(Platform.isIOS) {
        if(value == const Color(0XFF373332)) {
          return Brightness.dark;
        }
        else {
          return Brightness.light;
        }
      }
      else {
        if(value == const Color(0XFF373332)) {
          return Brightness.light;
        }
        else {
          return Brightness.light;
        }
      }
    } catch (e) {
      if(value == const Color(0XFF373332)) {
        return Brightness.light;
      }
      else {
        return Brightness.dark;
      }
    }
  }
}