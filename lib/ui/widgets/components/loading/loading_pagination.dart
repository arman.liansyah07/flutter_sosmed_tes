import 'package:flutter/material.dart';

class LoadingPagination extends StatelessWidget {
  const LoadingPagination({super.key});

  @override
  Widget build(BuildContext context) {
    return  Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.fromLTRB(20, 20, 20, 40),
      child: const CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Color(0xffE3E9ED)),
        backgroundColor: Color(0XFF312b46),
        strokeWidth: 5.0,
      ),
    );
  }
}
