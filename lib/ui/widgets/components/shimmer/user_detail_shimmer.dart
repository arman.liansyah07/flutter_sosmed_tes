import 'package:flutter/material.dart';
import 'package:kazokku_apps/ui/widgets/components/shimmer/box_shimmer.dart';

import '../../../../core/configs/base_colors.dart';
import '../../../routes/route_page.dart';
import 'post_shimmer.dart';

class UserDetailShimer extends StatelessWidget {
  const UserDetailShimer({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: 120,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg1.png"),
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
          child: Row(
            children: [
              InkWell(
                onTap: () {
                  RoutePage.back();
                },
                child: Container(
                  padding: const EdgeInsets.fromLTRB(20, 20, 10, 10),
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: BaseColors.white,
                    size: 15,
                  ),
                ),
              ),
              const Expanded(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 10, 20, 0),
                  child: BoxShimmer(
                    height: 40,
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 60, 0, 0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(105),
                        child: const BoxShimmer(
                          height: 80,
                          width: 80,
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(color: const Color(0XFFF9F9F9), borderRadius: BorderRadius.circular(10)),
                        child: const Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            BoxShimmer(
                              width: 130,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  flex: 4,
                                  child: BoxShimmer(
                                    width: 130,
                                  ),
                                ),
                                SizedBox(width: 20),
                                Expanded(
                                  flex: 6,
                                  child: BoxShimmer(
                                    width: 130,
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  flex: 4,
                                  child: BoxShimmer(
                                    width: 130,
                                  ),
                                ),
                                SizedBox(width: 20),
                                Expanded(
                                  flex: 6,
                                  child: BoxShimmer(
                                    width: 130,
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            BoxShimmer(
                              width: double.infinity,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                  child: const Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      BoxShimmer(
                        width: 130,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      PostShimmer()
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
