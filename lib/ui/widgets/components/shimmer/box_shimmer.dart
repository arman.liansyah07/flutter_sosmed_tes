import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../../../core/configs/base_colors.dart';

class BoxShimmer extends StatelessWidget {
  final double width;
  final double height;
  final double radius;
  const BoxShimmer({
    this.width = 150,
    this.height = 15,
    this.radius = 10,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: BaseColors.blueOverlay,
      highlightColor: BaseColors.bgScreen,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(radius),
        ),
      ),
    );
  }
}
