import 'package:flutter/material.dart';

import '../../../../core/configs/base_colors.dart';
import 'box_shimmer.dart';

class PostShimmer extends StatelessWidget {
  const PostShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: 5,
      itemBuilder: (context, index) {
        return Container(
            padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 20),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  blurRadius: 10,
                  color: Colors.black.withOpacity(.01),
                  offset: const Offset(0, 0),
                ),
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(105),
                      child: const BoxShimmer(
                        width: 35,
                        height: 35,
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    const Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          BoxShimmer(
                            width: 150,
                            height: 15,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          BoxShimmer(
                            width: 35,
                            height: 15,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: const BoxShimmer(
                    width: double.infinity,
                    height: 180,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Wrap(
                  children: ["1", "2", "3"].map((e) {
                    return Container(
                        margin: const EdgeInsets.only(right: 10),
                        padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                        decoration: BoxDecoration(color: BaseColors.blue, borderRadius: BorderRadius.circular(10)),
                        child: const BoxShimmer(
                          width: 40,
                          height: 30,
                        ));
                  }).toList(),
                ),
                const SizedBox(
                  height: 10,
                ),
                const BoxShimmer(
                  width: double.infinity,
                  height: 20,
                ),
                const SizedBox(
                  height: 5,
                ),
                const BoxShimmer(
                  width: 100,
                  height: 20,
                ),
              ],
            ));
      },
    );
  }
}
