import 'package:flutter/material.dart';
import 'package:kazokku_apps/ui/screens/welcome/dashboard_screen.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'core/configs/base_fonts.dart';
import 'core/utils/local_database_utils.dart';
import 'core/utils/navigation_utils.dart';
import 'core/utils/provider_utils.dart';
import 'core/utils/storage_utils.dart';
import 'ui/routes/injector.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  LocalDatabaseUtils localDb = LocalDatabaseUtils.instance;
  await StorageUtils.init();
  List<SingleChildWidget>? providers = await ProviderUtils.register();

  runApp(MyApp(providers: providers));
}

class MyApp extends StatelessWidget {
  final List<SingleChildWidget>? providers;
  const MyApp({Key? key, @required this.providers}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers!,
      child: MaterialApp(
        navigatorKey: locator<NavigationUtils>().navigatorKey,
        title: "Sosmed",
        home: const DashboardScreen(),
        theme: ThemeData(fontFamily: BaseFonts.poppinsRegular),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
