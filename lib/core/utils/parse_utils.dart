import 'dart:convert';
import 'package:intl/intl.dart';

class ParseUtils {
  ParseUtils._();

  static String castString(var object, [String defaultValue = '']) {
    if (object is int) {
      object = object.toString();
    }
    return object == null ? defaultValue : object as String;
  }

  static int castInt(var object, [int defaultValue = 0]) {
    if (object == null) {
      return defaultValue;
    } else {
      if (object is num) {
        return (object).toInt();
      } else if (object is String) {
        return int.tryParse(object) ?? defaultValue;
      } else {
        return defaultValue;
      }
    }
  }

  static double castDouble(var object, [double defaultValue = 0]) {
    if (object == null) {
      return defaultValue;
    } else {
      if (object is num) {
        return (object).toDouble();
      } else if (object is String) {
        return double.tryParse(object) ?? defaultValue;
      } else {
        return defaultValue;
      }
    }
  }

  static bool castBool(var object, [bool defaultValue = false]) {
    if (object is String) {
      if (object == "") {
        return false;
      } else if (object == "0") {
        return false;
      } else if (object == "1") {
        return true;
      } else if (object == "true") {
        return true;
      } else if (object == "false") {
        return false;
      }
    }
    if (object is int) {
      return object == 0 ? false : true;
    }
    return object == null ? defaultValue : (object as bool);
  }

  static List castList(var object, [List defaultValue = const []]) {
    return object == null ? defaultValue : object as List;
  }

  static Map castMap(var object, [Map defaultValue = const {}]) {
    if (object is String) {
      try {
        return Map.from(json.decode(object));
      } catch (e) {
        return defaultValue;
      }
    }

    return object == null ? defaultValue : object as Map;
  }

  static String castDateDB(var inputDate) {
    DateFormat inputFormat = DateFormat('dd-MM-yyyy');
    DateFormat outputFormat = DateFormat('yyyy-MM-dd');

    DateTime date = inputFormat.parse(inputDate);
    String formattedDate = outputFormat.format(date);
    return formattedDate;
  }

  static DateTime? castDate8601(var object, [DateTime? defaultValue]) {
    if (object == null && defaultValue == null) return null;
    if (!object is String || object.isEmpty) return null;

    DateFormat result = DateFormat("yyyy-MM-dd'T'hh:mm:ss");
    return result.parse(object);
  }

  static String castToRupiah(var object) {
    if (object is String) {
      object = int.parse(object);
    } else if (object == null) {
      return "";
    }
    return NumberFormat('Rp#,##0', 'ID').format(object);
  }

  static String castToNumber(var object) {
    if (object is String) {
      var clearObject = object.replaceAll(".", "");
      clearObject = clearObject.replaceAll(",", ".");
      object = num.parse(clearObject);
    } else if (object == null) {
      return "";
    }
    return NumberFormat('#,##0', 'ID').format(object);
  }

  static String castToDateFormat(var object, {bool isTime = false}) {
    String dateFormat = "dd MMM yyyy";
    if (isTime) {
      dateFormat = "dd MMM yyyy HH:mm";
    }
    String result = "";
    if (object != null) {
      if (object == "") {
        result = DateFormat(dateFormat).format(DateTime.now());
      } else {
        final utc = DateTime.parse(object).toUtc();
        final serverTime = utc.add(const Duration(hours: 7));
        result = DateFormat(dateFormat).format(serverTime);
      }
    }
    return result;
  }

  static List<T> castListType<T>(json, Function(Map<String, dynamic>) create) {
    var data = <T>[];
    if (json != null) {
      json.forEach((v) {
        data.add(create(v));
      });
    }
    return data;
  }
}
