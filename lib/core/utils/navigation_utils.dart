import 'package:flutter/material.dart';

import '../../ui/routes/injector.dart';

final navigate = locator<NavigationUtils>();

class NavigationUtils {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
}
