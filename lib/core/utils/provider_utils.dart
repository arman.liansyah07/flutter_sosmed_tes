import 'package:kazokku_apps/core/viewmodels/post_saved_provider.dart';
import 'package:kazokku_apps/core/viewmodels/setting_provider.dart';
import 'package:kazokku_apps/core/viewmodels/user_post_provider.dart';
import 'package:kazokku_apps/core/viewmodels/user_providers.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import '../viewmodels/post_provider.dart';

class ProviderUtils {
  ProviderUtils._();

  /// Register your provider here
  static Future<List<SingleChildWidget>> register() async {
    return [
      ChangeNotifierProvider(create: (context) => SettingProvider()),
      ChangeNotifierProvider(create: (context) => UserProvider()),
      ChangeNotifierProvider(create: (context) => UserPostProvider()),
      ChangeNotifierProvider(create: (context) => PostSavedProvider()),
      ChangeNotifierProvider(create: (context) => PostProvider()),
    ];
  }
}
