import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class LocalDatabaseUtils {
  static final _databaseName = "kazokku.db";
  static final _databaseVersion = 1;

  static final columnId = 'id';

  LocalDatabaseUtils._privateConstructor();
  static final LocalDatabaseUtils instance = LocalDatabaseUtils._privateConstructor();

  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;

    // If _database is null, initialize it
    _database = await _initDatabase();
    return _database!;
  }

  // Create the database and the table
  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path, version: _databaseVersion, onCreate: _onCreate);
  }

  // Create the table
  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE posts (
        id TEXT PRIMARY KEY,
        json_data TEXT NOT NULL
      )
    ''');

    await db.execute('''
      CREATE TABLE friend_user (
        id TEXT PRIMARY KEY,
        json_data TEXT NOT NULL
      )
    ''');
  }

  // Insert operation
  Future<int> insert(String tableName, Map<String, dynamic> row) async {
    Database db = await instance.database;
    return await db.insert(tableName, row);
  }

  // Query operation
  Future<List<Map<String, dynamic>>> queryAll(String tableName) async {
    Database db = await instance.database;
    return await db.query(tableName);
  }

  // Update operation
  Future<int> update(String tableName, Map<String, dynamic> row) async {
    Database db = await instance.database;
    String id = row[columnId];
    return await db.update(tableName, row, where: '$columnId = ?', whereArgs: [id]);
  }

  // Delete operation
  Future<int> delete(String tableName, String id) async {
    Database db = await instance.database;
    return await db.delete(tableName, where: '$columnId = ?', whereArgs: [id]);
  }

  // Insert or update operation
  Future<int> insertOrUpdate(String tableName, Map<String, dynamic> row) async {
    String id = row[columnId];
    List<Map<String, dynamic>> rows = await queryById(tableName, id);

    if (rows.isNotEmpty) {
      // Data with the same ID already exists, update the existing data
      return await update(tableName, row);
    } else {
      // Data with the same ID does not exist, insert new data
      return await insert(tableName, row);
    }
  }

  Future<List<Map<String, dynamic>>> queryById(String tableName, String id) async {
    Database db = await instance.database;
    return await db.query(tableName, where: '$columnId = ?', whereArgs: [id], limit: 1);
  }
}
