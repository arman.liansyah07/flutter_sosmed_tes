import '../utils/parse_utils.dart';

class Location {
  String street;
  String city;
  String state;
  String country;
  String timezone;

  Location({
    required this.street,
    required this.city,
    required this.state,
    required this.country,
    required this.timezone,
  });

  factory Location.fromJson(Map<String, dynamic> json) {
    return Location(
      street: ParseUtils.castString(json["street"]),
      city: ParseUtils.castString(json["city"]),
      state:ParseUtils.castString(json["state"]),
      country: ParseUtils.castString(json["country"]),
      timezone: ParseUtils.castString(json["timezone"]),
    );
  }
}
