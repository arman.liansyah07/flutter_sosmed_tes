import 'package:kazokku_apps/core/utils/parse_utils.dart';

import 'user.dart';

class Post {
  String id;
  String image;
  int likes;
  List<String> tags;
  String text;
  String publishDate;
  User owner;
  bool isSave;

  Post({
    required this.id,
    required this.image,
    required this.likes,
    required this.tags,
    required this.text,
    required this.publishDate,
    required this.owner,
    required this.isSave,
  });

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      id: ParseUtils.castString(json["id"]),
      image: ParseUtils.castString(json["image"]),
      likes: ParseUtils.castInt(json["likes"]),
      tags: List<String>.from(json['tags'] as List),
      text: ParseUtils.castString(json["text"]),
      publishDate: json["publishDate"],
      owner: User.fromJson(json["owner"]),
      isSave: false,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'image': image,
      'likes': likes,
      'tags': tags,
      'text': text,
      'publishDate': publishDate,
      'owner': owner.toMap(),
    };
  }
}
