import '../utils/parse_utils.dart';
import 'location.dart';

class User {
  String id;
  String title;
  String firstName;
  String lastName;
  String picture;

  String? gender;
  String? dateOfBirth;
  String? registerDate;
  String? email;
  Location? address;

  bool isFriend;

  User({
    required this.id,
    required this.title,
    required this.firstName,
    required this.lastName,
    required this.picture,
    this.gender,
    this.dateOfBirth,
    this.registerDate,
    this.email,
    this.address,
    required this.isFriend,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: ParseUtils.castString(json["id"]),
      title: ParseUtils.castString(json["title"]),
      firstName: ParseUtils.castString(json["firstName"]),
      lastName: ParseUtils.castString(json["lastName"]),
      picture: ParseUtils.castString(json["picture"]),
      gender: json["gender"],
      dateOfBirth: json["dateOfBirth"],
      registerDate: json["registerDate"],
      email: json["email"],
      address: json["location"] != null ? Location.fromJson(json["location"]) : null,
      isFriend: false,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'firstName': firstName,
      'lastName': lastName,
      'picture': picture,
    };
  }
}
