import 'package:dio/dio.dart';
import 'package:kazokku_apps/core/configs/base_urls.dart';
import '../configs/base_methods.dart';

class BaseService {
  Options header = Options(headers: {
    "app-id": BaseUrls.appId,
  });

  Future<Response> run(String url, BaseMethods method, {dynamic data, bool useToken = false}) async {
    Dio dio = Dio();

    Response response;

    try {
      switch (method) {
        case BaseMethods.get:
          response = await dio.get(url, options: header);
          break;
        case BaseMethods.post:
          response = await dio.post(url, data: data, options: header);
          break;
        case BaseMethods.put:
          response = await dio.put(url, data: data, options: header);
          break;
        case BaseMethods.delete:
          response = await dio.delete(url, data: data, options: header);
          break;
      }
    } on DioException catch (e) {
      if (e.response != null && e.response!.data != null) {
        if (e.response!.data is String) {
          response = Response(requestOptions: e.requestOptions, statusCode: 500, statusMessage: "API Error : ${e.message}, Detail in ${e.response!.data}");
        } else {
          response = Response(requestOptions: e.requestOptions, statusCode: 500, statusMessage: "${e.response!.data!["message"]}");
        }
      } else {
        response = Response(requestOptions: e.requestOptions, statusCode: 500, statusMessage: "API Error : ${e.message}, Detail in ${e.response == null ? "" : e.response!.data}");
      }
    }
    return response;
  }
}
