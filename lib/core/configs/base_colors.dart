import 'package:flutter/material.dart';

class BaseColors {
  BaseColors._();

  static Color white = Colors.white;
  static Color black = const Color(0XFF0B1C2A);
  
  static Color primaryDark = const Color(0XFF1B4D39);
  static Color primary = const Color(0XFF26604a);
  static Color success = const Color(0XFF4caf50);
  static Color danger = const Color(0XFFdf251a);
  static Color warning = const Color(0xFFE3B04D);
  static Color bgScreen = const Color(0XFFF9F9F9);
  static Color blue = const Color(0XFF024185);
  static Color blueOverlay = const Color(0XFFAED1F2);
  static Color overlayBtn = const Color(0XFF9893a6);

  static Color textMenuColor = const Color(0XFF2B4156);
  static Color textColor = const Color(0XFF0D1433);
  static Color textSubColor = const Color(0XFF5A7590);

  static Color backgroundColor = const Color(0x00000000);
}