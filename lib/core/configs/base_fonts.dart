class BaseFonts {
  BaseFonts._();

  static String interBlack = "Inter-Black";
  static String interBold = "Inter-Bold";
  static String interExtraBold = "Inter-ExtraBold";
  static String interMedium = "Inter-Medium";
  static String interRegular = "Inter-Regular";
  static String interSemiBold = "Inter-SemiBold";

  static String poppinsBold = "Poppins-Bold";
  static String poppinsExtraBold = "Poppins-ExtraBold";
  static String poppinsMedium = "Poppins-Medium";
  static String poppinsRegular = "Poppins-Regular";
  static String poppinsSemiBold = "Poppins-SemiBold";
}
