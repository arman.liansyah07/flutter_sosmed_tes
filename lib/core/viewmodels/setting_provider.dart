import 'package:flutter/material.dart';
import 'package:kazokku_apps/ui/screens/posts/post_saved_screen.dart';
import 'package:kazokku_apps/ui/screens/posts/post_screen.dart';
import 'package:kazokku_apps/ui/screens/users/user_screen.dart';

class SettingProvider extends ChangeNotifier {
  List<Widget> listOfPages = [
    const UserScreen(),
    const PostScreen(),
    const PostSavedScreen(),
  ];

  int index = 0;
  void setPage(int value) {
    index = value;
    notifyListeners();
  }
}
