import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:kazokku_apps/core/configs/base_methods.dart';
import 'package:kazokku_apps/core/configs/base_urls.dart';
import 'package:kazokku_apps/core/services/base_services.dart';
import 'package:kazokku_apps/core/utils/parse_utils.dart';

import '../models/post.dart';
import '../utils/local_database_utils.dart';

class PostProvider extends ChangeNotifier {
  var service = BaseService();

  List<Post> listOfData = [];
  int limit = 20;
  int page = 0;

  bool showLoading = false;
  setShowLoading(bool value) {
    showLoading = value;
    notifyListeners();
  }

  bool showLoadingPage = false;
  setShowLoadingPage(bool value) {
    showLoadingPage = value;
    notifyListeners();
  }

  Future<void> getlist({bool isShowPagination = false}) async {
    if (isShowPagination) {
      page += 1;
      setShowLoadingPage(true);
    } else {
      page = 0;
      setShowLoading(true);
    }

    String baseUrl = "${BaseUrls.api}post";
    if(selectedTag != "") {
       baseUrl = "${BaseUrls.api}tag/${selectedTag}/post";
    } 
    var responseData = await service.run(
      baseUrl + "?limit=$limit&page=$page",
      BaseMethods.get,
    );

    if (responseData.statusCode == 200) {
      if (isShowPagination) {
        var dataNew = ParseUtils.castListType<Post>(responseData.data["data"], Post.fromJson);
        listOfData.addAll(dataNew);
      } else {
        listOfData = ParseUtils.castListType<Post>(responseData.data["data"], Post.fromJson);
      }
    }

    for (var row in listOfData) {
      LocalDatabaseUtils localDB = LocalDatabaseUtils.instance;
      var check = await localDB.queryById("posts", row.id);
      if (check.length != 0) {
        row.isSave = true;
      }
    }
    notifyListeners();
    if (isShowPagination) {
      setShowLoadingPage(false);
    } else {
      setShowLoading(false);
    }
  }

  Future<void> saveFavorite(int valueIndex) async {
    LocalDatabaseUtils localDB = LocalDatabaseUtils.instance;
    Map<String, dynamic> jsonData = {
      "id": listOfData[valueIndex].id,
      "json_data": jsonEncode(listOfData[valueIndex].toMap()),
    };

    var checkData = await localDB.queryById("posts", jsonData["id"]);
    if (checkData.length == 0) {
      await localDB.insert("posts", jsonData);
      listOfData[valueIndex].isSave = true;
      listOfData[valueIndex].likes += 1;
    } else {
      if (listOfData[valueIndex].isSave) {
        await localDB.delete("posts", listOfData[valueIndex].id);
        listOfData[valueIndex].isSave = false;
        listOfData[valueIndex].likes -= 1;
      }
    }
    notifyListeners();
  }

  String selectedTag = "";
  void setSelectedTag(String value) {
    selectedTag = value;
    notifyListeners();
    getlist();
  }
}
