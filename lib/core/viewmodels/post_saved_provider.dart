import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:kazokku_apps/core/models/post.dart';
import 'package:kazokku_apps/core/utils/local_database_utils.dart';

class PostSavedProvider extends ChangeNotifier {
  List<Post> listOfData = [];

  bool showLoading = false;
  setShowLoading(bool value) {
    showLoading = value;
    notifyListeners();
  }

  Future<void> getlist() async {
    setShowLoading(true);
    listOfData = [];
    LocalDatabaseUtils localDB = LocalDatabaseUtils.instance;
    List<Map<String, dynamic>> jsonRows = await localDB.queryAll("posts");
    for (var jsonRow in jsonRows) {
      var objectData = jsonDecode(jsonRow["json_data"]);
      var post = Post.fromJson(objectData);
      post.isSave = true;
      listOfData.add(post);
    }
    setShowLoading(false);
  }

  Future<void> saveFavorite(int valueIndex) async {
    LocalDatabaseUtils localDB = LocalDatabaseUtils.instance;
    Map<String, dynamic> jsonData = {
      "id": listOfData[valueIndex].id,
      "json_data": jsonEncode(listOfData[valueIndex].toMap()),
    };

    var checkData = await localDB.queryById("posts", jsonData["id"]);
    if (checkData.length == 0) {
      await localDB.insert("posts", jsonData);
      listOfData[valueIndex].isSave = true;
      listOfData[valueIndex].likes += 1;
    } else {
      if (listOfData[valueIndex].isSave) {
        await localDB.delete("posts", listOfData[valueIndex].id);
        listOfData[valueIndex].isSave = false;
        listOfData[valueIndex].likes -= 1;
      }
    }
    getlist();
    notifyListeners();
  }
}
