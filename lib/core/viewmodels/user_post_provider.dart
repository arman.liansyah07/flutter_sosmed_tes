import 'dart:convert';

import 'package:flutter/material.dart';

import '../configs/base_methods.dart';
import '../configs/base_urls.dart';
import '../models/post.dart';
import '../services/base_services.dart';
import '../utils/local_database_utils.dart';
import '../utils/parse_utils.dart';

class UserPostProvider extends ChangeNotifier {
  var service = BaseService();

  List<Post> listOfDataTemp = [];
  List<Post> listOfData = [];
  int limit = 20;
  int page = 0;

  bool showLoading = false;
  setShowLoading(bool value) {
    showLoading = value;
    notifyListeners();
  }

  bool showLoadingPage = false;
  setShowLoadingPage(bool value) {
    showLoadingPage = value;
    notifyListeners();
  }

  TextEditingController search = TextEditingController();
  Future<void> getlist(String userId, {bool isShowPagination = false, bool isSearch = false}) async {
    if (isSearch && search.text != "") {
      listOfData = listOfDataTemp.where((e) => e.text.toLowerCase().contains(search.text.toLowerCase())).toList();
      notifyListeners();
    } else {
      if (isShowPagination) {
        page += 1;
        setShowLoadingPage(true);
      } else {
        page = 0;
        setShowLoading(true);
      }

      var responseData = await service.run(
        "${BaseUrls.api}user/$userId/post?limit=$limit&page=$page",
        BaseMethods.get,
      );

      if (responseData.statusCode == 200) {
        if (isShowPagination) {
          var dataNew = ParseUtils.castListType<Post>(responseData.data["data"], Post.fromJson);
          listOfData.addAll(dataNew);
        } else {
          listOfData = ParseUtils.castListType<Post>(responseData.data["data"], Post.fromJson);
        }
      }

      for (var row in listOfData) {
        LocalDatabaseUtils localDB = LocalDatabaseUtils.instance;
        var check = await localDB.queryById("posts", row.id);
        if (check.length != 0) {
          row.isSave = true;
        }
      }

      listOfDataTemp = listOfData;
      notifyListeners();
      if (isShowPagination) {
        setShowLoadingPage(false);
      } else {
        setShowLoading(false);
      }
    }
  }

  Future<void> saveFavorite(int valueIndex) async {
    LocalDatabaseUtils localDB = LocalDatabaseUtils.instance;
    Map<String, dynamic> jsonData = {
      "id": listOfData[valueIndex].id,
      "json_data": jsonEncode(listOfData[valueIndex].toMap()),
    };

    var checkData = await localDB.queryById("posts", jsonData["id"]);
    if (checkData.length == 0) {
      await localDB.insert("posts", jsonData);
      listOfData[valueIndex].isSave = true;
      listOfData[valueIndex].likes += 1;
    } else {
      if (listOfData[valueIndex].isSave) {
        await localDB.delete("posts", listOfData[valueIndex].id);
        listOfData[valueIndex].isSave = false;
        listOfData[valueIndex].likes -= 1;
      }
    }
    var check = listOfDataTemp.where((e) => e.id == listOfData[valueIndex].id).toList();
    if (check.length != 0) {
      int indexTemp = listOfDataTemp.indexOf(check.first);
      listOfDataTemp[indexTemp] = listOfData[valueIndex];
    }
    notifyListeners();
  }

}
