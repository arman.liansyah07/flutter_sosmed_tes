import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:kazokku_apps/core/configs/base_methods.dart';
import 'package:kazokku_apps/core/configs/base_urls.dart';
import 'package:kazokku_apps/core/models/user.dart';
import 'package:kazokku_apps/core/services/base_services.dart';
import 'package:kazokku_apps/core/utils/local_database_utils.dart';
import 'package:kazokku_apps/core/utils/parse_utils.dart';


class UserProvider extends ChangeNotifier {
  var service = BaseService();

  List<User> listOfData = [];
  int limit = 20;
  int page = 0;

  bool showLoading = false;
  setShowLoading(bool value) {
    showLoading = value;
    notifyListeners();
  }

  bool showLoadingPage = false;
  setShowLoadingPage(bool value) {
    showLoadingPage = value;
    notifyListeners();
  }

  Future<void> getlist({bool isShowPagination = false}) async {
    if (isShowPagination) {
      page += 1;
      setShowLoadingPage(true);
    } else {
      page = 0;
      setShowLoading(true);
    }

    var responseData = await service.run(
      "${BaseUrls.api}user?limit=$limit&page=$page",
      BaseMethods.get,
    );

    if (responseData.statusCode == 200) {
      if (isShowPagination) {
        var dataNew = ParseUtils.castListType<User>(responseData.data["data"], User.fromJson);
        listOfData.addAll(dataNew);
      } else {
        listOfData = ParseUtils.castListType<User>(responseData.data["data"], User.fromJson);
      }
    }

    notifyListeners();
    if (isShowPagination) {
      setShowLoadingPage(false);
    } else {
      setShowLoading(false);
    }
  }

  User? user;
  Future<void> getDetail(String id) async {
    user = null;
    notifyListeners();
    var responseData = await service.run(
      "${BaseUrls.api}user/$id",
      BaseMethods.get,
    );

    if (responseData.statusCode == 200) {
      user = User.fromJson(responseData.data);
      LocalDatabaseUtils localDB = LocalDatabaseUtils.instance;
       var check = await localDB.queryById("friend_user", user!.id);
        if (check.length != 0) {
          user!.isFriend = true;
        }
    }
    notifyListeners();
  }

  Future<void> addFriend() async {
    LocalDatabaseUtils localDB = LocalDatabaseUtils.instance;
    Map<String, dynamic> jsonData = {
      "id": user!.id,
      "json_data": jsonEncode(user!.toMap()),
    };

    var checkData = await localDB.queryById("friend_user", jsonData["id"]);
    if (checkData.length == 0) {
      await localDB.insert("friend_user", jsonData);
      user!.isFriend = true;
    } else {
      if (user!.isFriend) {
        await localDB.delete("friend_user", user!.id);
        user!.isFriend = false;
      }
    }
    notifyListeners();
  }
}
